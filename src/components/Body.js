import React from 'react'
import '../css/Body.css';
import Header from './Header';
import { useDataLayerValue } from '../context-API/DataLayer';
import { FavoriteOutlined, MoreHoriz, PlayCircleFilled } from '@mui/icons-material';
import SongRow from './SongRow';

function Body({spotify}) {
    const [{discover_weekly}, dispatcher] = useDataLayerValue();
    discover_weekly.tracks.items.forEach(element => {
        console.log(element.track);
    });
    return (
        <div className="_body">
           <Header spotify={spotify}/>
           <div className="body__info">
               <img src={discover_weekly?.images[0].url} alt="Padimaj" />
               <div className="body__infoText">
                   <strong>PLAYLIST</strong>
                   <h2>{discover_weekly?.name}</h2>
                   <p>{discover_weekly?.description}</p>
               </div>
           </div>
           <div className="body__songs">
               <div className="body__icons">
                   <PlayCircleFilled className="body__shuffle"/>
                   <FavoriteOutlined fontSize="large" />
                   <MoreHoriz />
               </div>

               {discover_weekly?.tracks.items.map(item => 
                 (  <SongRow track={item.track} />)
               )}
           </div>
        </div>
    )
}

export default Body
